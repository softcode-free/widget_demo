import 'package:flutter/material.dart';

class AccountView extends StatefulWidget {
  @override
  _AccountViewState createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  bool isDelay = false;

  delay() {
    Future.delayed(const Duration(seconds: 10), () {
      setState(() {
        isDelay = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    delay();
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Colors.brown,
          drawer: Drawer(),
          appBar: AppBar(
            toolbarHeight: 180,
            backgroundColor: Colors.brown,
            iconTheme: IconThemeData(color: Colors.black),
            elevation: 0,
            title: Container(
              child: TextField(
                decoration: InputDecoration(
                    hoverColor: Colors.white,
                    fillColor: Colors.white,
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1),
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(30.0),
                        )),
                    focusedBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1),
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(30.0),
                        )),
                    hintText: 'Search for food',
                    hintStyle: TextStyle(color: Colors.white)),
              ),
            ),
            flexibleSpace: Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(top: 140, left: 20),
              child: PreferredSize(
                preferredSize: Size(0, 20),
                child: Text(
                  "Fresh Tasty Burger",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
              ),
            ),
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.directions_car)),
                Tab(icon: Icon(Icons.directions_transit)),
                Tab(icon: Icon(Icons.directions_bike)),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Wrap(
                spacing: 8.0,
                children: [
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                  Chip(label: Text("data1")),
                ],
              ),
              isDelay == false
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : DataTable(
                      dataRowColor: MaterialStateColor.resolveWith(
                          (states) => Colors.blue),
                      headingRowColor: MaterialStateColor.resolveWith(
                          (states) => Colors.yellow),
                      columns: [
                          DataColumn(
                              label: Text(
                            "Id",
                            style: TextStyle(color: Colors.white),
                          )),
                          DataColumn(label: Text("Name")),
                          DataColumn(label: Text("Salary")),
                        ],
                      rows: [
                          DataRow(cells: [
                            DataCell(Text("001")),
                            DataCell(Text("Kimsoer")),
                            DataCell(Text("1k")),
                          ]),
                          DataRow(cells: [
                            DataCell(Text("002")),
                            DataCell(Text("Nut")),
                            DataCell(Text("2k")),
                          ])
                        ]),
              Icon(Icons.directions_bike),
            ],
          ),
        ));
  }
}
