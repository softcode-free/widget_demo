import 'package:flutter/material.dart';
import 'package:widget_demo/account_view.dart';
import 'package:widget_demo/main_screen_view.dart';
import 'package:widget_demo/my_class_view.dart';
import 'package:widget_demo/setting_view.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  List<String> list = ["One", "Two", "Three"];

  List<Widget> widgetList = [
    MainScreenView(),
    MyClassView(),
    AccountView(),
    SettingView()
  ];
  int index = 0;
  String dropdownValue = "One";
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: widgetList.elementAt(index),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: index,
        onTap: (index) {
          setState(() {
            this.index = index;
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              label: "Home"),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.tv,
            ),
            label: "My Class",
          ),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: "Account"),
          BottomNavigationBarItem(icon: Icon(Icons.settings), label: "Setting")
        ],
      ),
    );
  }
}
