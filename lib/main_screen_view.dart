import 'package:flutter/material.dart';

class MainScreenView extends StatefulWidget {
  @override
  _MainScreenViewState createState() => _MainScreenViewState();
}

class _MainScreenViewState extends State<MainScreenView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: Image.asset(
                  "lib/asset/banner.jpg",
                  height: 200,
                  fit: BoxFit.cover,
                ),
              ),
              GridView.count(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  crossAxisCount: 4,
                  crossAxisSpacing: 2.0,
                  mainAxisSpacing: 2.0,
                  children: List.generate(50, (index) {
                    return InkWell(
                      onTap: () {
                        Scaffold.of(context).showBottomSheet((context) {
                          return Container(
                            color: Colors.blue,
                            height: 200,
                            width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.only(bottom: 20),
                            child: Column(
                              children: [
                                Text(
                                  "Hello Bottomsheet",
                                  style: TextStyle(color: Colors.white),
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text("Closed"))
                              ],
                            ),
                          );
                        });
                      },
                      onDoubleTap: () {
                        showModalBottomSheet(
                            context: context,
                            builder: (index) {
                              return Container(
                                color: Colors.blue,
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(bottom: 20),
                                child: Column(
                                  children: [
                                    Text(
                                      "Hello Modal Bottomsheet",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text("Closed"))
                                  ],
                                ),
                              );
                            });
                      },
                      onLongPress: () {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Hello world"),
                            action: SnackBarAction(
                              label: "Action",
                              onPressed: () {
                                print("go to home");
                              },
                            )));
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        color: Colors.amber,
                        child: Card(
                          elevation: 20,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                backgroundColor: Colors.brown,
                                child: Image.asset(
                                  "lib/asset/beltei_logo.png",
                                  width: 20,
                                  height: 20,
                                ),
                              ),
                              Text("ពត៌មាន")
                            ],
                          ),
                        ),
                      ),
                    );
                  }))
            ],
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width * 0.5,
            child: Card(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      "សិក្សាតាមប្រពន័្ធ អេឡិចត្រូចនិច E-learning",
                      maxLines: 3,
                    ),
                    width: 100,
                  ),
                  Image.asset(
                    "lib/asset/pc.jpg",
                    height: 80,
                    width: 80,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
