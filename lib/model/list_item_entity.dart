import 'package:flutter/cupertino.dart';

class ListItemEntity {
  String title;
  IconData iconData;

  ListItemEntity({this.title, this.iconData});
}
