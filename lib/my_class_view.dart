import 'package:flutter/material.dart';
import 'package:widget_demo/model/list_item_entity.dart';

class MyClassView extends StatefulWidget {
  @override
  _MyClassViewState createState() => _MyClassViewState();
}

class _MyClassViewState extends State<MyClassView> {
  List<ListItemEntity> list = List();

  @override
  void initState() {
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 12", iconData: Icons.school));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 13", iconData: Icons.business));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 14", iconData: Icons.bubble_chart));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 15", iconData: Icons.seven_k));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 16", iconData: Icons.settings));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 12", iconData: Icons.school));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 13", iconData: Icons.business));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 14", iconData: Icons.bubble_chart));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 15", iconData: Icons.seven_k));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 16", iconData: Icons.settings));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 12", iconData: Icons.school));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 13", iconData: Icons.business));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 14", iconData: Icons.bubble_chart));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 15", iconData: Icons.seven_k));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 16", iconData: Icons.settings));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 12", iconData: Icons.school));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 13", iconData: Icons.business));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 14", iconData: Icons.bubble_chart));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 15", iconData: Icons.seven_k));
    list.add(ListItemEntity(
        title: "ចំណេះដឹងទូទៅថ្នាក់ទី 16", iconData: Icons.settings));
  }

  bool isExpand = false;
  int isShow = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Expansion Panel List"),
        ),
        body: SingleChildScrollView(
          child: ExpansionPanelList(
            expansionCallback: (index, isExpanded) {
              setState(() {
                print("index=> $index  $isExpanded");
                if (isExpanded == false) {
                  isExpand = true;
                } else {
                  isExpand = false;
                }
              });
            },
            children: [
              ExpansionPanel(
                  headerBuilder: (index, isExpanded) {
                    return Container(
                        height: 50,
                        color: Colors.blue,
                        child: ListTile(
                          title: Text("Item show"),
                        ));
                  },
                  body: ListTile(
                    title: Text("Hello detail"),
                    subtitle: Text("Full Description"),
                  ),
                  isExpanded: this.isExpand)
            ],
          ),
        ));
  }
}
