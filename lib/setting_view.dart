import 'package:flutter/material.dart';

class SettingView extends StatefulWidget {
  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 100,
            width: 100,
            color: Colors.amber,
          ),
          Expanded(
            flex: 2,
            child: Container(
              height: 100,
              width: 100,
              color: Colors.grey,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              height: 100,
              width: 100,
              color: Colors.deepPurple,
            ),
          ),
          Container(
            height: 100,
            child: Row(
              children: [
                Container(
                  color: Colors.blue,
                  height: 100,
                  width: 100,
                ),
                Expanded(
                  child: Container(
                    color: Colors.green,
                    height: 100,
                    width: 100,
                  ),
                ),
                Container(
                  color: Colors.lightGreen,
                  height: 100,
                  width: 100,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
